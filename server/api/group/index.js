'use strict';

var express = require('express');
var controller = require('./group.controller');
var auth = require('../../auth/auth.service');


var router = express.Router();
var sitesRouter = express.Router({mergeParams: true});

// you can nest routers by attaching them as middleware
router.use('/', sitesRouter);

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/:id', controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.update);
router.patch('/:id', auth.hasRole('admin'), controller.update);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);

// sites
sitesRouter.post('/:id/sites', controller.createSite);
sitesRouter.get('/:id/sites', controller.showSite);

module.exports = router;
