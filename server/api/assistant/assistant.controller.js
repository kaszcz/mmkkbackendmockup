'use strict';

var _ = require('lodash');
var Assistant = require('./assistant.model');

exports.index = function(req, res) {
    var resObjList = [];

    Assistant.find(function (err, assistants) {
        if(err) { return handleError(res, err); }
        _.forOwn(assistants, function(value, key) {
            var resObj = convertToAssistantListObject(value);
            resObjList.push(resObj);
        });
        console.log(JSON.stringify(resObjList));
        return res.status(200).json(resObjList);
    });

};

function convertToAssistantListObject(mongooseObj) {
    var obj = {};
    obj.assistantId = mongooseObj.assistantId;
    obj.userName = mongooseObj.userName;
    return obj;
}

function handleError(res, err) {
    return res.send(500, err);
};