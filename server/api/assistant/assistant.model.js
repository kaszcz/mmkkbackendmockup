'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AssistantSchema = new Schema({
    // header
    assistantId: String,
    userName: String
});


// Ensure virtual fields are serialised.
AssistantSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Assistant', AssistantSchema);

