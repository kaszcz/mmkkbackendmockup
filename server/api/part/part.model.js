'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PartSchema = new Schema({
    // header
    _id: Number,
    orderNo: {type: Number, index: true},
    partNumber: {type: String, index: true},
    description: String,
    qty: Number,
    IC: String,
    PO: String,
    WMS: String,
    jobNo: String,
    oldSN: String,
    newSN: String
});

// Duplicate the ID field.
//OrderSchema.virtual('id').get(function() {
//    var id = mongoose.Types.ObjectId(this._id);
//    return parseInt(this._id);
//});

// Ensure virtual fields are serialised.
PartSchema.set('toJSON', {
    virtuals: true
});

module.exports = mongoose.model('Part', PartSchema);
