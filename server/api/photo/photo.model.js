'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PhotoSchema = new Schema({
    orderNo: {type: Number, index: true},
    data: Buffer,
    contentType: String
});

module.exports = mongoose.model('Photo', PhotoSchema);
