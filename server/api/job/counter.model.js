'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');

var CounterSchema = new Schema({
    "orderNo": {type: Number, index: true},
    "nextSeqNumber": Number
});

CounterSchema.plugin(findOrCreate);

CounterSchema.pre('save', function(next) {
    this.nextSeqNumber += 10;
    next();
});

module.exports = mongoose.model('Counter', CounterSchema);
