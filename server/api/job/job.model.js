'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var findOrCreate = require('mongoose-findorcreate');
var Counter = require('./counter.model');


var JobSchema = new Schema({
    id: Number,
    orderNo: Number,
    timestamp: Number,
    jobId: Number,
    jobNo: Number,
    jobName: String
});

JobSchema.index({ orderNo: 1, jobNo: 1 }, { unique: true });

JobSchema.plugin(findOrCreate);

// Autoincrement
//JobSchema.pre('save', function(next) {
//    var doc = this;
//
//    Counter.findOne({orderNo: doc.orderNo}, {}, { sort: { 'created_at' : -1 } }, function(err, counter) {
//        if(err) { return handleError(res, err); }
//
//        counter.save({orderNo: doc.orderNo}, function (err) {
//            if (err) return handleError(err);
//
//            doc.jobNo = counter.nextSeqNumber-10;
//            next();
//        })
//    });
//
//});

module.exports = mongoose.model('Job', JobSchema);

function handleError(res, err) {
    return res.status(500).json({ "message": err.message });
}