'use strict';

exports.version = function(req, res) {
    return res.status(200).json({"version": "0.6.2"});
};


function handleError(res, err) {
    return res.send(500, err);
};