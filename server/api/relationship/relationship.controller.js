'use strict';

var _ = require('lodash');
var Relationship = require('./relationship.model');
var Group = require('../group/group.model');
var Device = require('../device/device.model');

// Get list of relationships
exports.index = function (req, res) {
  Relationship.find(function (err, relationships) {
    if (err) {
      return handleError(res, err);
    }
    return res.json(200, relationships);
  });
};

// Get a single relationship
//GET {{rootURL}}/api/relationships/abc123
exports.show = function (req, res) {

  Group.findOne({code: req.params.id}, function (err, group) {
    if (err) {
      return handleError(res, err);
    }
    if (!group) {
      return res.send(404);
    }

    //return sum of relationships for each pair of sites
    var valueMatch = new RegExp(req.params.id);
    Relationship.aggregate(
      {
        $match : {groupCode : { $regex: valueMatch} }
      },
      {
        $project: {
          groupCode: 1,
          siteCodeTo: 1,
          relationshipCount: 1
        }
      },
      {
        $group: {
          _id: {groupCode: "$groupCode", siteCodeTo: "$siteCodeTo"},
          relCnt: {$sum: "$relationshipCount"}
        }
      }
    ).exec(function (err, resp) {
        if (err) return handleError(err);

        // create a set of sites to have unique site names
        var siteSet = { };
        resp.forEach(function(entry) {
          siteSet[entry._id.groupCode] = true;
          siteSet[entry._id.siteCodeTo] = true;
        });
        console.log("siteSet=" + JSON.stringify(siteSet));
        // convert set of sites to site list
        var siteList = [];
        for(var key in siteSet) siteList.push(key);
        console.log("siteList=" + JSON.stringify(siteList));

        // create empty dependency matrix
        var xMax = siteList.length;
        var matrix = new Array(xMax);
        for (var y = 0; y < xMax; y++) {
          matrix[y] = new Array(xMax);
        }
        // initiate the matrix
        for (var y = 0; y < xMax; y++) {
          for (var x = 0; x < xMax; x++) {
            matrix[x][y] = 0;
          }
        }
        // fill the matrix with data
        resp.forEach(function(entry) {
          var x = siteList.indexOf(entry._id.groupCode);
          var y = siteList.indexOf(entry._id.siteCodeTo);
          matrix[x][y] = entry.relCnt;
        });

        // prepare json response
        var respObj = {
          packageNames: siteList,
          matrix: matrix
        };
        console.log("respObj=" + respObj);

        //var dataTrans = {
        //  packageNames: ['PL', 'FR', 'US'],
        //  matrix: [
        //    [10975, 5871, 8916],
        //    [1951, 10048, 2060],
        //    [8010, 16145, 8090]]
        //};

        return res.json(respObj);
      });

  });

};

// Creates a new relationship in the DB.
// POST
// {{rootURL}}/api/relationship/
//{
//  "groupCode": "abc123/pl",
//  "timestamp": "2015-02-24",
//  "relationshipCount": 15,
//  "siteCodeTo": "abc123/fr"
//  "uuid": "5f3546fe-3754-4a20-b918-d515ad2f8aaf"
//}
exports.create = function (req, res) {

  Group.findOne({code: req.body.groupCode}, function (err, group) {
    if (err) {
      return handleError(res, err);
    }
    if (!group) {
      return res.send(404);
    }

    //Check if your mood has been updated today
    if (req.body.uuid) {
      // if it has uuid then check if it updated server today
      Device.findOne({uuid: req.body.uuid}, function (err, device) {
        if (err) {
          return handleError(res, err);
        }
        if (!device) {
          return res.send(404, {notifications: 'DEVICE_ID_NOT_FOUND'});
        }

        Relationship.findOrCreate(
          {
            groupCode: req.body.groupCode,
            timestamp: req.body.timestamp,
            siteCodeTo: req.body.siteCodeTo,
            uuid: req.body.uuid
          }, function (err, relationship, created) {
            if (err) {
              return handleError(res, err);
            }

            if (relationship._id) {
              delete relationship._id
            }
            if (relationship.__v) {
              delete relationship.__v
            }

            var relationshipRes = {
              notifications: created ? "CREATED_OK" : "UPDATED_OK",
              relationship: {
                timestamp: relationship.timestamp,
                groupCode: relationship.groupCode,
                siteCodeTo: relationship.siteCodeTo,
                relationshipCount: req.body.relationshipCount
              }
            };

            //add moodLevel field
            relationship.relationshipCount = req.body.relationshipCount;
            relationship.save(function (err) {
              if (err) {
                return handleError(res, err);
              }
            });

            // Update devices list
            return res.json(201, relationshipRes);
          });

      });

    } else {
      return res.send(401, {notifications: 'DEVICE_ID_NOT_FOUND'});
    }


  });

};

// Updates an existing relationship in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Relationship.findById(req.params.id, function (err, relationship) {
    if (err) {
      return handleError(res, err);
    }
    if (!relationship) {
      return res.send(404);
    }
    var updated = _.merge(relationship, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.json(200, relationship);
    });
  });
};

// Deletes a relationship from the DB.
exports.destroy = function (req, res) {
  Relationship.findById(req.params.id, function (err, relationship) {
    if (err) {
      return handleError(res, err);
    }
    if (!relationship) {
      return res.send(404);
    }
    relationship.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
