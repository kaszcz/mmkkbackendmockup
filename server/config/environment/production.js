'use strict';

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip:       process.env.OPENSHIFT_NODEJS_IP ||
  process.env.IP ||
  undefined,

  // Server port
  port:     process.env.OPENSHIFT_NODEJS_PORT ||
  process.env.PORT ||
  8080,

  seedDB: true,

  // MongoDB connection options
  mongo: {
    uri:    process.env.MONGOLAB_URL ||
            process.env.MONGOHQ_URL ||
            process.env.OPENSHIFT_MONGODB_DB_URL+process.env.OPENSHIFT_APP_NAME ||
            process.env.CUSTOMCONNSTR_AZURE_PROD_URL,

    options: {
      server:{
        auto_reconnect: true,
        poolSize: 10,
        socketOptions:{
          keepAlive: 1
        }
      },
      db: {
        numberOfRetries: 10,
        retryMiliSeconds: 1000
      }
    }

  }
};

