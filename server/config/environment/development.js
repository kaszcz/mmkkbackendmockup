'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // Server IP
  ip: "127.0.0.1",

  // Server port
  port:     process.env.OPENSHIFT_NODEJS_PORT ||
  process.env.PORT ||
  9000,

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/mmkk-dev'
  },

  seedDB: true
};
