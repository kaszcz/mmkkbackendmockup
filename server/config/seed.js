/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');
var Order = require('../api/order/order.model');
var History = require('../api/history/history.model');
var Part = require('../api/part/part.model');
var Counter = require('../api/job/counter.model');
var Job = require('../api/job/job.model');
var Trip = require('../api/job/trip.model');
var Assistant = require('../api/assistant/assistant.model');
var AssistantTrip = require('../api/job/assistant.trip.model');

Order.find({}).remove(function() {
    Order.create({
            _id: 1,
            orderNo: 7654388,
            MAT: "003",
            orderStatus: "CRTD",
            failure: "test",
            smsContent: "sms content 1",
            customerName: "박정주",
            contactNumber: "017-610-7201",
            model: "EC55C",
            machineLocation: "서울",
            machineSN: "122007",
            createdDate: 1441206000,
            plannedDate: 1441206000,
            closedDate: 0,
            HMR: 0,
            breakDown: true,
            mechanic: null,
            contactPerson: "Johnny Bravo",
            createdDatetime: 1441206000,
            plannedDatetime: 1441206000,
            deliveryDatetime: 1441206000,
            closedDatetime: 1441206000,
            attentionForMachine: "This is my remark for machine",
            attentionForCustomer: "This is my remark for customer"
        },
        {
            _id: 2,
            orderNo: 7613126,
            MAT: "001",
            orderStatus: "TECO",
            failure: "운전실 비상 탈출용 망치 거치대 파손",
            smsContent: "sms content 2",
            customerName: "금강건기",
            contactNumber: "01054341418",
            model: "EC55C",
            machineLocation: "",
            machineSN: "125222",
            createdDate: 1431529200,
            plannedDate: Math.floor(Date.now() / 1000),
            closedDate: 0,
            HMR: 0,
            breakDown: false,
            mechanic: null,
            contactPerson: "Lukasz Kaszczyszyn",
            createdDatetime: 1441206000,
            plannedDatetime: 1441206000,
            deliveryDatetime: 1441206000,
            closedDatetime: 1441206000,
            attentionForMachine: "This is my remark for machine",
            attentionForCustomer: "This is my remark for customer"
        },
        {
            _id: 3,
            orderNo: 7613027,
            MAT: "022",
            orderStatus: "TECO",
            failure: "에어컨작동불",
            smsContent: "sms content 3",
            customerName: "장승호",
            contactNumber: "01054630714",
            model: "EC55C",
            machineLocation: "",
            machineSN: "121536",
            createdDate: 1431442800,
            plannedDate: 1468800000,
            closedDate: 0,
            HMR: 0,
            breakDown: false,
            mechanic: null,
            contactPerson: "Damian Kowalczyk",
            createdDatetime: 1441206000,
            plannedDatetime: 1441206000,
            deliveryDatetime: 1441206000,
            closedDatetime: 1441206000,
            attentionForMachine: "This is my remark for machine",
            attentionForCustomer: "This is my remark for customer"
        },
        {
            _id: 4,
            orderNo: 7613005,
            MAT: "022",
            orderStatus: "REL",
            failure: "MCV부 오일누유",
            smsContent: "sms content 4",
            customerName: "금강건기",
            contactNumber: "01036758100",
            model: "EC55C",
            machineLocation: "",
            machineSN: "124499",
            createdDate: 1431442800,
            plannedDate: 0,
            closedDate: 0,
            HMR: 0,
            breakDown: false,
            mechanic: null,
            contactPerson: "Damian Kowalczyk",
            createdDatetime: 1441206000,
            plannedDatetime: 1441206000,
            deliveryDatetime: 1441206000,
            closedDatetime: 1441206000,
            attentionForMachine: "This is my remark for machine",
            attentionForCustomer: "This is my remark for customer"
        }, function() {
            console.log('finished populating orders');
        });
});

History.find({}).remove(function() {
    History.create({
            _id: 1,
            orderNo: 7654388,
            MAT: "003",
            orderStatus: "CLSD",
            failure: "Engine was broken",
            customerName: "박정주",
            contactNumber: "017-610-7201",
            model: "EC55C",
            machineLocation: "서울",
            machineSN: "122007",
            createdDate: 1441206000,
            plannedDate: 1441206000,
            closedDate: 0,
            HMR: 0,
            breakDown: true,
            mechanic: null
        },
        {
            _id: 2,
            orderNo: 7654388,
            MAT: "003",
            orderStatus: "CLSD",
            failure: "Oil leakage",
            customerName: "박정주",
            contactNumber: "017-610-7201",
            model: "EC55C",
            machineLocation: "서울",
            machineSN: "122007",
            createdDate: 1431442800,
            plannedDate: 1431442800,
            closedDate: 0,
            HMR: 0,
            breakDown: true,
            mechanic: null
        },
        function() {
            console.log('finished populating history');
        });
});

Part.find({}).remove(function() {
    Part.create({
            "_id": 0,
            "orderNo": 7654388,
            "partNumber": "22216525",
            "description": "CAMSHAFT_CAMSHAFT (DEUTZ)",
            "qty": 1,
            "IC": "L",
            "PO": "2012-02-24 08:28:55",
            "WMS": "2012-02-27 13:22:53",
            "jobNo": "0010",
            "oldSN": "",
            "newSN": ""
        },
        {
            "_id": 1,
            "orderNo": 7654388,
            "partNumber": "7014536078",
            "description": "ENGINE_EC290BP_STEPIII",
            "qty": 1,
            "IC": "L",
            "PO": "2012-02-25 08:32:55",
            "WMS": "2012-02-27 13:25:53",
            "jobNo": "0010",
            "oldSN": "",
            "newSN": ""
        },
        {
            "_id": 2,
            "orderNo": 7654388,
            "partNumber": "2345643",
            "description": "Big Wheel",
            "qty": 2,
            "IC": "L",
            "PO": "2012-02-22 09:15:51",
            "WMS": "2012-02-24 12:15:53",
            "jobNo": "0010",
            "oldSN": "",
            "newSN": ""
        },
        function() {
            console.log('finished populating parts');
        });
});

Job.find({}).remove(function() {
    Job.create({
            //"_id": 1,
            "orderNo": 7654388,
            "timestamp": Math.floor(Date.now()),
            "jobId": 0,
            "jobNo": 10,
            "jobName": "RESISTOR_560OHM ASSY FOR PU",
        },
        function() {
            console.log('finished populating jobs');
        });
});

Trip.find({}).remove(function() {
    Trip.create({
            "tripDate": 1469923200,
            "orderNo": 7654388,
            "jobNo": 10,
            "tripNo": 1,
            "tripId" : 0
        },
        function() {
            console.log('finished populating trips');
        });
});

Assistant.find({}).remove(function() {
    Assistant.create({
            "assistantId":  "EC1234",
            "userName" : "Johnny Bravo",
        },
        {
            "assistantId":  "EC20505",
            "userName" : "John Doe",
        },
        function() {
            console.log('finished populating assistants');
        });
});

AssistantTrip.find({}).remove(function() {
    AssistantTrip.create({
            "orderNo": 7654388,
            "jobNo": 10,
            "tripNo":  1,

            "assistantId":  "EC1234",
            "tripDate": 1470009600,
            "drivingHours": 3.5,
            "drivingMileage": 120,
            "waitingHours": 1.5,
            "workingHours": 4.5
        },
        function() {
            console.log('finished populating assistant trips');
        });
});


//Thing.find({}).remove(function() {
//    Thing.create({
//        name : 'Development Tools',
//        info : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.'
//    }, {
//        name : 'Server and Client integration',
//        info : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.'
//    }, {
//        name : 'Smart Build System',
//        info : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html'
//    },  {
//        name : 'Modular Structure',
//        info : 'Best practice client and server structures allow for more code reusability and maximum scalability'
//    },  {
//        name : 'Optimized Build',
//        info : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.'
//    },{
//        name : 'Deployment Ready',
//        info : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators'
//    }, function() {
//        console.log('finished populating things');
//    });
//});


User.find({}).remove(function() {
    User.create({
            provider: 'local',
            name: 'Test User',
            email: 'test@test.com',
            password: 'test'
        }, {
            provider: 'local',
            role: 'admin',
            name: 'Admin',
            email: 'admin@admin.com',
            password: 'admin'
        }, function() {
            console.log('finished populating users');
        }
    );
});