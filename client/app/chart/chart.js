'use strict';

angular.module('moodAppBackendApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('chart', {
        url: '/charts',
        templateUrl: 'app/chart/chart.html',
        controller: 'ChartCtrl',
        authenticate: true,
        resolve: {
          groupList: function($q, ChartService) {
            var deferred = $q.defer();
            ChartService.getGroupList(function(data) {
              deferred.resolve(data);
            });
            return deferred.promise;
          }
        }
      })
      .state('chart.details', {
        url: '/:id',
        templateUrl: 'app/chart/chart.details.html',
        controller: 'ChartDetailsCtrl',
        authenticate: true
      })
      .state('publicchart', {
        url: '/chart/:id',
        templateUrl: 'app/chart/public.chart.details.html',
        controller: 'PublicChartDetailsCtrl',
        authenticate: false
      });

  });
