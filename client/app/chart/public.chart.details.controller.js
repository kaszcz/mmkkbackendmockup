'use strict';

angular.module('moodAppBackendApp')
  .controller('PublicChartDetailsCtrl', function ($scope, ChartService, $stateParams) {

    $scope.activeButton = "week";

    $scope.isActive = function(route) {
      return route === $scope.activeButton;
    };

    $scope.init = function() {
      console.log('public.chart.init : ' + $stateParams.id);
      $scope.groupCode = $stateParams.id;

      $scope.getOneWeekData();

      $scope.options = {
        chart: {
          type: 'multiBarChart',
          height: 500,
          margin: {
            top: 20,
            right: 20,
            bottom: 60,
            left: 45
          },
          //tooltip: function(key, y, e, graph) {
          //  return '' + e + ' at ' + y;
          //},
          clipEdge: true,
          staggerLabels: true,
          transitionDuration: 500,
          stacked: false,
          showControls: false,
          xAxis: {
            axisLabel: 'Date',
            axisLabelDistance: 40,
            showMaxMin: false,
            tickFormat: function (d) {
              //return d3.format(',f')(d);
              //var dx = $scope.data[0].values[d].x;
              //console.log(label + ', ' + dx);
              var label = $scope.data[0].values[d-1].label?$scope.data[0].values[d-1].label:d;
              console.log('label=' + label);
              return label;

            }
          },
          yAxis: {
            axisLabel: 'Mood Level',
            axisLabelDistance: 40,
            tickFormat: function (d) {
              return d3.format(',.0f')(d);
            }
          }
        }
      }

    }; // init()


    $scope.getAllData = function() {
      console.log("getAllData");
      $scope.activeButton = "all";
      ChartService.getAllData($scope.groupCode, function(data) {
        $scope.data = data;
      });

    }; //getAllData

    $scope.getYearData = function() {
      console.log("getYearData");
      $scope.activeButton = "year";
      ChartService.getTwelveMonthsData($scope.groupCode, function(data) {
        $scope.data = data;
      });
    }; //getYearData

    $scope.getThreeMonthsData = function() {
      console.log("getThreeMonthsData");
      $scope.activeButton = "3m";
      ChartService.getThreeMonthsData($scope.groupCode, function(data) {
        $scope.data = data;
      });
    }; //getThreeMonthsData

    $scope.getOneMonthData = function() {
      console.log("getOneMonthData");
      $scope.activeButton = "month";

      ChartService.getOneMonthData($scope.groupCode, function(data) {
        $scope.data = data;
      });

    }; //getOneMonthData

    $scope.getOneWeekData = function() {
      console.log("getOneWeekData");
      $scope.activeButton = "week";

      ChartService.getOneWeekData($scope.groupCode, function(data) {
        $scope.data = data;
      });

    }; //getOneWeekData

  });

