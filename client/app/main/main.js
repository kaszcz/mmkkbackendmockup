'use strict';

angular.module('moodAppBackendApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/groups/groups.html',
        controller: 'GroupsCtrl',
        authenticate: true
      });
      //.state('main', {
      //  url: '/',
      //  templateUrl: 'app/main/main.html',
      //  controller: 'MainCtrl'
      //});
  });
