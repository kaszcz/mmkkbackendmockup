'use strict';

angular.module('moodAppBackendApp')
  .controller('InteractionsCtrl', function ($scope, SitesService, $stateParams, $http, $window) {
      console.log('InteractionsCtrl.init : ' + $stateParams.id);
      console.log("D3: " + JSON.stringify($window.d3) );

      $scope.groupCode = $stateParams.id;
      //$scope.groupCode = 'abc123';

      SitesService.getChordData($scope.groupCode, function(chordData) {
        //$scope.chordData = chordData;
        $scope.dependencyWheelChart = $window.d3.chart.dependencyWheel();
          //.width(700)    // also used for height, since the wheel is in a a square
          //.margin(100)   // used to display package names
          //.padding(.02); // separating groups in the wheel;

        $window.d3.select('#interactions_chart_lg')
          .datum(chordData)
          .call($scope.dependencyWheelChart.width(800).margin(100).padding(.02));

        $window.d3.select('#interactions_chart_md')
          .datum(chordData)
          .call($scope.dependencyWheelChart.width(500).margin(100).padding(.02));

        $window.d3.select('#interactions_chart_sm')
          .datum(chordData)
          .call($scope.dependencyWheelChart.width(290).margin(100).padding(.02));
      });



  });
