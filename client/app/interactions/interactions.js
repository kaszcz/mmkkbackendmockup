'use strict';

angular.module('moodAppBackendApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('interactions', {
        url: '/interactions/:id',
        templateUrl: 'app/interactions/interactions.html',
        controller: 'InteractionsCtrl'
      });
  });
