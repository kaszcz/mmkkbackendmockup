'use strict';

angular.module('moodAppBackendApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sites', {
        url: '/sites',
        templateUrl: 'app/sites/sites.html',
        controller: 'SitesCtrl',
        authenticate: true,
        resolve: {
          groupList: function($q, SitesService) {
            var deferred = $q.defer();
            SitesService.getGroupList(function(data) {
              deferred.resolve(data);
            });
            return deferred.promise;
          }
        }
      })
      .state('sites.details', {
        url: '/:id',
        templateUrl: 'app/sites/sites.details.html',
        controller: 'SitesDetailsCtrl',
        authenticate: true
      });

  });
